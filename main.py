from flask_sqlalchemy import SQLAlchemy

#Pour gérer les mdp de manière hachée
from werkzeug.security import generate_password_hash, check_password_hash

from flask import render_template, request, jsonify
#from flask_login import login_required,current_user
from sqlalchemy.orm import sessionmaker

import json


# app/article/views.py

from flask import  Flask, abort, flash, redirect, render_template, url_for, jsonify,json,request
from flask_login import current_user, login_required

from flask_uploads import configure_uploads, IMAGES, UploadSet
from werkzeug.utils import secure_filename
import os


app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://postgres:moucalino@db/E-mirabeau'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.secret_key = 'jknjknc6v468v86v354'


db = SQLAlchemy(app)


class Categorie(db.Model):
    """
    Create an Article table
    """

    # Ensures table will be named in plural and not in singular
    # as is the name of the model
    __tablename__ = 'categories'

    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(60))
    sexe = db.Column(db.String(60))
    nomsexe = db.Column(db.Text)
    articles = db.relationship('Article', backref='categorie',
                                lazy='dynamic')


    def __repr__(self):
        return '<Categorie: {}>'.format(self.id)




class Article(db.Model):
    """
    Create an Article table
    """

    # Ensures table will be named in plural and not in singular
    # as is the name of the model
    __tablename__ = 'articles'

    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(60), index=True)
    description = db.Column(db.Text)
    photo = db.Column(db.Text)
    categorie_id = db.Column(db.Integer,db.ForeignKey('categories.id'))
    date_publication = db.Column(db.Date)
    stocks = db.relationship('Stock', backref='article',
                                lazy='dynamic')


    def __repr__(self):
        return '<Article: {}>'.format(self.nom)



class Stock(db.Model):
    """
    Create an Article table
    """

    # Ensures table will be named in plural and not in singular
    # as is the name of the model
    __tablename__ = 'stocks'

    id = db.Column(db.Integer, primary_key=True)
    id_article = db.Column(db.Integer,db.ForeignKey('articles.id'))
    taille = db.Column(db.String(60))
    nom_couleur = db.Column(db.String(60))
    couleur = db.Column(db.String(60))
    prix_in = db.Column(db.Float)
    prix_fin = db.Column(db.Float)
    solde = db.Column(db.Integer)
    quantite = db.Column(db.Integer)
    photos = db.relationship('Photo', backref='photo',
                                lazy='dynamic')

    def __repr__(self):
        return '<Stock: {}>'.format(self.id)


class Photo(db.Model):
    """
    Create an Article table
    """

    # Ensures table will be named in plural and not in singular
    # as is the name of the model
    __tablename__ = 'photos'

    id = db.Column(db.Integer, primary_key=True)
    id_stock = db.Column(db.Integer, db.ForeignKey('stocks.id'))
    lien_photo = db.Column(db.String(60))
    position = db.Column(db.Integer)


    def __repr__(self):
        return '<Photo: {}>'.format(self.id)


class Avis(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_article = db.Column(db.Integer, db.ForeignKey('articles.id'))
    note = db.Column(db.Integer)
    commentaire = db.Column(db.Text)
    id_utilisateur = db.Column(db.Integer)

    def __repr__(self):
        return '<Avis : {}'.format(self.id)

@app.route('/h')
def hello_world():
    return '<h1> Hello AAD </h1>'

@app.route('/test')
def hello_AAD():
    return '<h1> Hello AAD, welcome to test </h1>'


@app.route('/')
def index():
    articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.photo,Stock.prix_in,Stock.solde,Stock.prix_fin).limit(20)
    table_articles = []
    for article in articles:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_articles.append(json_com)

    categories = Categorie.query.distinct('nom').all()
    table_categories = []
    for categorie in categories:
        json_com={
            'nom_categorie': categorie.nom,
            'nom-sexe': categorie.nomsexe,
            'sexe': categorie.sexe
        }
        table_categories.append(json_com)

    marticles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.photo,Stock.prix_in,Stock.solde,Stock.prix_fin,Avis.note).order_by('note').limit(10)
    table_marticles = []
    for article in marticles:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_marticles.append(json_com)

    response = jsonify({"articles": table_articles,'categories': table_categories,'best-seller': table_marticles})
    return response.json

@app.route('/detail/<int:id>')
def detail(id):
    article = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.categorie_id,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(id=id)
    for a in article:
        idcat = a.categorie_id

    categories = Categorie.query.filter_by(id=idcat).distinct('nom')
    articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).limit(20).all()
    avis = Avis.query.filter_by(id_article=id)

    table_article = []
    for article in article:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'description': article.description,
            'photo': article.photo,
            'couleur': article.nom_couleur,
            'taille': article.taille,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'quantite': article.quantite

        }
        table_article.append(json_com)

    table_avis = []
    for av in avis:
        json_com={
            'note': av.note,
            'commentaire': av.commentaire
        }
        table_avis.append(json_com)


    table_articles = []
    for article in articles:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_articles.append(json_com)

    table_categories = []
    for categorie in categories:
        json_com={
            'nom_categorie': categorie.nom,
            'nom-sexe': categorie.nomsexe,
            'sexe': categorie.sexe
        }
        table_categories.append(json_com)

    marticles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.photo,Stock.prix_in,Stock.solde,Stock.prix_fin,Avis.note).order_by('note').limit(10)

    table_marticles = []
    for article in marticles:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_marticles.append(json_com)

    response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"avis": table_avis,"best-seller": table_marticles})
    return response.json

@app.route('/recherche/<int:page>',methods=['POST'])
def recherche():
    if request.method == 'POST':
        if request.form.get('tri') == 'prix':
            articler = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter(Article.nom.like("%"+request.form.get('recherche')+"%")).order_by("prix_in").paginate(page,per_page=24)
            categorie = Categorie.query.filter_by(id=idcat).distinct('nom')
            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            table_articler = []
            for article in articler.items:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            response = jsonify({"articler": table_articler,"articles": table_articles,"categories": table_categories})
            return response.json

        if request.form.get('tri') == "produit":
            articler = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter(Article.nom.like("%"+request.form.get('recherche')+"%")).order_by("nom").paginate(page,per_page=24)
            categorie = Categorie.query.filter_by(id=idcat).distinct('nom')
            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            table_articler = []
            for article in articler:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            response = jsonify({"articler": table_articler,"articles": table_articles,"categories": table_categories})
            return response.json

    articler = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter(Article.nom.like("%"+request.form.get('recherche')+"%")).paginate(page,per_page=24)
    categorie = Categorie.query.filter_by(id=idcat).distinct('nom')
    articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)

    table_articles = []
    for article in articles:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_articles.append(json_com)

    table_categories = []
    for categorie in categories:
        json_com={
            'nom_categorie': categorie.nom,
            'nom-sexe': categorie.nomsexe,
            'sexe': categorie.sexe
        }
        table_categories.append(json_com)

    table_articler = []
    for article in articler:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_articles.append(json_com)

    response = jsonify({"articler": table_articler,"articles": table_articles,"categories": table_categories})
    return response.json



@app.route("/produits/<int:page>",methods=['POST','GET'])
def produits(page):
    if request.method == 'POST':
        if request.form.get('tri') == 'prix':
            articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).order_by('prix_in').paginate(page,per_page=24)
            categories = Categorie.query.distinct('nom').all()
            nbreArticles = 0
            for article in articles.items:
                nbreArticles = nbreArticles + 1
            table_articles = []
            for article in articles.items:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            nbarticles = {'nombre articles': nbreArticles}

            marticles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.photo,Stock.prix_in,Stock.solde,Stock.prix_fin,Avis.note).order_by('note').limit(10)
            table_marticles = []
            for article in marticles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_marticles.append(json_com)


            response = jsonify({"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,"best-seller": table_marticles})
            return response.json


        if request.form.get('tri') == 'produit':
            articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).order_by('nom').paginate(page,per_page=24)
            categories = Categorie.query.distinct('nom').all()
            nbreArticles = 0
            for article in articles.items:
                nbreArticles = nbreArticles + 1
            table_articles = []
            for article in articles.items:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            nbarticles = {'nombre articles': nbreArticles}
            marticles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.photo,Stock.prix_in,Stock.solde,Stock.prix_fin,Avis.note).order_by('note').limit(10)
            table_marticles = []
            for article in marticles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_marticles.append(json_com)

            response = jsonify({"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,"best-seller": marticles})
            return response.json

    articles = Article.query.join(Stock,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).paginate(page,per_page=24)
    categories = Categorie.query.distinct('nom').all()
    nbreArticles = 0
    for article in articles.items:
        nbreArticles = nbreArticles + 1
    table_articles = []
    for article in articles.items:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_articles.append(json_com)

    table_categories = []
    for categorie in categories:
        json_com={
            'nom_categorie': categorie.nom,
            'nom-sexe': categorie.nomsexe,
            'sexe': categorie.sexe
        }
        table_categories.append(json_com)

    nbarticles = {'nombre articles': nbreArticles}

    marticles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.photo,Stock.prix_in,Stock.solde,Stock.prix_fin,Avis.note).order_by('note').limit(10)
    table_marticles = []
    for article in marticles:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_marticles.append(json_com)


    response = jsonify({"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,"best-seller": table_marticles})
    return response.json


@app.route("/sexe/<s>/<int:page>")
def sexe(s,page):

    if request.method == 'POST':
        if request.form.get('tri') == 'prix':
            sexe = Categorie.query.filter_by(sexe=s)

            articlec = Stock.query.join(Article,Stock.id_article==Article.id).join(Categorie,Categorie.id==Article.categorie_id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Categorie.sexe).filter_by(sexe=s).order_by('prix_in').paginate(page,per_page=24)

            s = s

            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
            nbreArticles = 0

            for article in articles:
                nbreArticles = nbreArticles + 1

            categories = Categorie.query.distinct('nom').all()

            table_article = []
            for article in articlec.items:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_article.append(json_com)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            nbarticles = {'nombre articles': nbreArticles}

            catégorie_sexe = {'sexe': s}
            response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,"categorie_sexe": categorie_sexe})
            return response.json

        if request.form.get('tri') == 'produit':
            sexe = Categorie.query.filter_by(sexe=s)

            articlec = Stock.query.join(Article,Stock.id_article==Article.id).join(Categorie,Categorie.id==Article.categorie_id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Categorie.sexe).filter_by(sexe=s).order_by('prix_in').paginate(page,per_page=24)

            s = s

            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
            nbreArticles = 0
            for article in articles:
                nbreArticles = nbreArticles + 1

            categories = Categorie.query.distinct('nom').all()

            table_article = []
            for article in articlec.items:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_article.append(json_com)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            nbarticles = {'nombre articles': nbreArticles}
            catégorie_sexe = {'sexe': s}
            response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,"categorie_sexe": categorie_sexe})
            return response.json

    sexe = Categorie.query.filter_by(sexe=s)

    articlec = Stock.query.join(Article,Stock.id_article==Article.id).join(Categorie,Categorie.id==Article.categorie_id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Categorie.sexe).filter_by(sexe=s).order_by('prix_in').paginate(page,per_page=24)

    s = s

    articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
    nbreArticles = 0

    for article in articles:
        nbreArticles = nbreArticles + 1

    categories = Categorie.query.distinct('nom').all()

    table_article = []
    for article in articlec.items:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_article.append(json_com)

    table_articles = []
    for article in articles:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_articles.append(json_com)

    table_categories = []
    for categorie in categories:
        json_com={
            'nom_categorie': categorie.nom,
            'nom-sexe': categorie.nomsexe,
            'sexe': categorie.sexe
        }
        table_categories.append(json_com)

    nbarticles = {'nombre articles': nbreArticles}
    categorie_sexe = {'sexe': s}
    response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,"categorie_sexe": categorie_sexe})

    return response.json



@app.route("/category/<c>/<int:page>")
def category(c,page):
    if request.method == 'POST':
        if request.form.get('tri') == 'prix':
            cat = Categorie.query.filter_by(nom=c)
            idcategory = 0
            for c in cat:
                idcategory=c.id

            articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.categorie_id,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(categorie_id=idcategory).order_by('prix_in').paginate(page,per_page=24)
            nbreArticles = 0
            for article in articlec.items:
                nbreArticles = nbreArticles + 1

            c = c.nom

            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
            categories = Categorie.query.distinct('nom').all()

            table_article = []
            for article in articlec.items.items:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_article.append(json_com)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            nbarticles = {'nombre articles': nbreArticles}
            categorie = {'categorie': c}
            response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,"categorie": categorie})

            return response.json

        if request.form.get('tri') == 'produit':
            cat = Categorie.query.filter_by(nom=c)
            idcategory = 0
            for c in cat:
                idcategory=c.id

            articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.categorie_id,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(categorie_id=idcategory).order_by('nom').paginate(page,per_page=24)
            nbreArticles = 0
            for article in articlec.items:
                nbreArticles = nbreArticles + 1

            c = c.nom

            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
            categories = Categorie.query.distinct('nom').all()

            table_article = []
            for article in articlec.items:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_article.append(json_com)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            nbarticles = {'nombre articles': nbreArticles}
            categorie = {'categorie': c}
            response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,"categorie": categorie})

            return response.json

    cat = Categorie.query.filter_by(nom=c)
    idcategory = 0
    for c in cat:
        idcategory=c.id

    articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.categorie_id,Article.description,Article.photo,Article.description,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(categorie_id=idcategory).paginate(page,per_page=24)
    nbreArticles = 0
    for article in articlec.items:
        nbreArticles = nbreArticles + 1

    c = c

    articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
    categories = Categorie.query.distinct('nom').all()

    table_article = []
    for article in articlec.items:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_article.append(json_com)

    table_articles = []
    for article in articles:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_articles.append(json_com)

    table_categories = []
    for categorie in categories:
        json_com={
            'nom_categorie': categorie.nom,
            'nom-sexe': categorie.nomsexe,
            'sexe': categorie.sexe
        }
        table_categories.append(json_com)

    nbarticles = {'nombre articles': nbreArticles}
    categorie = {'categorie': c}
    response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,"categorie": categorie})

    return response.json


@app.route('/couleur/<color>/<int:page>')
def couleur(color,page):
    if request.method == 'POST':
        if request.form.get('tri') == 'prix':
            nom_couleur = Stock.query.filter_by(nom_couleur=color)
            idart = 0

            for c in nom_couleur:
                idart = c.id_article

            articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(id=idart).order_by('prix_in').paginate(page,per_page=24)
            nbreArticles = 0
            for article in articlec.items:
                nbreArticles = nbreArticles + 1

            c = color

            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
            categories = Categorie.query.distinct('nom').all()

            table_article = []
            for article in articlec.items:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_article.append(json_com)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            nbarticles = {'nombre articles': nbreArticles}
            couleur = {'couleur': c}
            response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,"couleur": couleur})

            return response.json

        if request.form.get('tri') == 'produit':
            nom_couleur = Stock.query.filter_by(nom_couleur=color)
            idart = 0

            for c in nom_couleur:
                idart = c.id_article

            articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(id=idart).order_by('nom').paginate(page,per_page=24)
            nbreArticles = 0
            for article in articlec.items:
                nbreArticles = nbreArticles + 1

            c = color

            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
            categories = Categorie.query.distinct('nom').all()

            table_article = []
            for article in articlec.items:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_article.append(json_com)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            nbarticles = {'nombre articles': nbreArticles}
            couleur = {'couleur': c}
            response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,"couleur": couleur})

            return response.json

    nom_couleur = Stock.query.filter_by(nom_couleur=color)
    idart = 0

    for c in nom_couleur:
        idart = c.id_article

    articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(id=idart).paginate(page,per_page=24)
    nbreArticles = 0
    for article in articlec.items:
        nbreArticles = nbreArticles + 1

    c = color

    articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
    categories = Categorie.query.distinct('nom').all()

    table_article = []
    for article in articlec.items:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_article.append(json_com)

    table_articles = []
    for article in articles:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_articles.append(json_com)

    table_categories = []
    for categorie in categories:
        json_com={
            'nom_categorie': categorie.nom,
            'nom-sexe': categorie.nomsexe,
            'sexe': categorie.sexe
        }
        table_categories.append(json_com)

    nbarticles = {'nombre articles': nbreArticles}
    couleur = {'couleur': c}
    response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,"couleur": couleur})

    return response.json


@app.route('/categorie_couleur/<categ>/<color>/<int:page>')
def categorie_couleur(categ,color,page):
    if request.method == 'POST':
        if request.form.get('tri') == 'prix':
            nom_couleur = Stock.query.filter_by(nom_couleur=color)
            idart = 0

            for c in nom_couleur:
                idart = c.id_article

            cat = Categorie.query.filter_by(nom=categ)
            idcategory = 0
            for c in cat:
                idcategory=c.id

            articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(id=idart).filter_by(categorie_id=idcategory).order_by('prix_in').paginate(page,per_page=24)
            nbreArticles = 0
            for article in articlec.items:
                nbreArticles = nbreArticles + 1

            c = color

            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
            categories = Categorie.query.distinct('nom').all()

            table_article = []
            for article in articlec.items:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_article.append(json_com)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            nbarticles = {'nombre articles': nbreArticles}
            categorie_couleur = {'categorie': categ,'couleur': c}
            response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,'categorie_couleur': categorie_couleur})

            return response.json


        if request.form.get('tri') == 'produit':
            nom_couleur = Stock.query.filter_by(nom_couleur=color)
            idart = 0

            for c in nom_couleur:
                idart = c.id_article

            cat = Categorie.query.filter_by(nom=categ)
            idcategory = 0
            for c in cat:
                idcategory=c.id

            articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(id=idart).filter_by(categorie_id=idcategory).order_by('produit').paginate(page,per_page=24)

            nbreArticles = 0
            for article in articlec.items:
                nbreArticles = nbreArticles + 1

            c = color

            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
            categories = Categorie.query.distinct('nom').all()


            table_article = []
            for article in articlec.items:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_article.append(json_com)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            nbarticles = {'nombre articles': nbreArticles}
            categorie_couleur = {'categorie': categ,'couleur': c}
            response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,'categorie_couleur': categorie_couleur})

            return response.json


    nom_couleur = Stock.query.filter_by(nom_couleur=color)
    idart = 0

    for c in nom_couleur:
        idart = c.id_article

    cat = Categorie.query.filter_by(nom=categ)
    idcategory = 0
    for c in cat:
        idcategory=c.id

    articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(id=idart).filter_by(categorie_id=idcategory).paginate(page,per_page=24)

    nbreArticles = 0
    for article in articlec.items:
        nbreArticles = nbreArticles + 1

    c = color

    articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
    categories = Categorie.query.distinct('nom').all()

    table_article = []
    for article in articlec.items:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_article.append(json_com)

    table_articles = []
    for article in articles:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_articles.append(json_com)

    table_categories = []
    for categorie in categories:
        json_com={
            'nom_categorie': categorie.nom,
            'nom-sexe': categorie.nomsexe,
            'sexe': categorie.sexe
        }
        table_categories.append(json_com)

    nbarticles = {'nombre articles': nbreArticles}
    categorie_couleur = {'categorie': categ,'couleur': c}
    response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,'categorie_couleur': categorie_couleur})

    return response.json

@app.route("/categorie_sexe/<categ>/<s>/<int:page>")
def categorie_sexe(categ,s,page):
    if request.method == 'POST':
        if request.form.get('tri') == 'prix':
            sexe = Categorie.query.filter_by(sexe=s).filter_by(nom=categ)

            articlec = ""
            for s in sexe:
                articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(categorie_id=s.id).order_by('prix_in').paginate(page,per_page=24)

            s = s

            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
            nbreArticles = 0
            for article in articles:
                nbreArticles = nbreArticles + 1
            categories = Categorie.query.distinct('nom').all()

            table_article = []
            for article in articlec.items:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_article.append(json_com)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            nbarticles = {'nombre articles': nbreArticles}
            categorie_sexe = {'categorie': categ,'sexe': s}
            response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,'categorie_sexe': categorie_sexe})

            return response.json

        if request.form.get('tri') == 'produit':
            sexe = Categorie.query.filter_by(sexe=s).filter_by(nom=categ)

            articlec = ""
            for s in sexe:
                articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(categorie_id=s.id).order_by('produit').paginate(page,per_page=24)

            s = s

            articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
            nbreArticles = 0
            for article in articlec.items:
                nbreArticles = nbreArticles + 1
            categories = Categorie.query.distinct('nom').all()

            table_article = []
            for article in articlec.items:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_article.append(json_com)

            table_articles = []
            for article in articles:
                json_com={
                    'id_article': article.id,
                    'nom_article': article.nom,
                    'prix': article.prix_in,
                    'solde': article.solde,
                    'prix_fin': article.prix_fin,
                    'photo': article.photo
                }
                table_articles.append(json_com)

            table_categories = []
            for categorie in categories:
                json_com={
                    'nom_categorie': categorie.nom,
                    'nom-sexe': categorie.nomsexe,
                    'sexe': categorie.sexe
                }
                table_categories.append(json_com)

            nbarticles = {'nombre articles': nbreArticles}
            categorie_sexe = {'categorie': categ,'sexe': s}
            response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,'categorie_sexe': categorie_sexe})

            return response.json


    sexe = Categorie.query.filter_by(sexe=s).filter_by(nom=categ)

    articlec = ""
    for s in sexe:
        articlec = Stock.query.join(Article,Stock.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite).filter_by(categorie_id=s.id).paginate(page,per_page=24)

    s = s

    articles = Article.query.join(Stock,Stock.id_article==Article.id).join(Avis,Avis.id_article==Article.id).add_columns(Article.id,Article.nom,Article.description,Article.description,Article.photo,Stock.prix_in,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.prix_fin,Stock.solde,Stock.quantite,Avis.note,Avis.commentaire).order_by('note').limit(20)
    nbreArticles = 0
    for article in articlec.items:
        nbreArticles = nbreArticles + 1
    categories = Categorie.query.distinct('nom').all()

    table_article = []
    for article in articlec.items:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_article.append(json_com)

    table_articles = []
    for article in articles:
        json_com={
            'id_article': article.id,
            'nom_article': article.nom,
            'prix': article.prix_in,
            'solde': article.solde,
            'prix_fin': article.prix_fin,
            'photo': article.photo
        }
        table_articles.append(json_com)

    table_categories = []
    for categorie in categories:
        json_com={
            'nom_categorie': categorie.nom,
            'nom-sexe': categorie.nomsexe,
            'sexe': categorie.sexe
        }
        table_categories.append(json_com)

    nbarticles = {'nombre articles': nbreArticles}
    categorie_sexe = {'categorie': categ,'sexe': s}
    response = jsonify({"article": table_article,"articles": table_articles,"categories": table_categories,"nbarticles": nbarticles,'categorie_sexe': categorie_sexe})

    return response.json

app.config['UPLOADED_IMAGES_DEST_article'] = '/Users/bayedamethiam/Desktop/STAGE VOLKENO/test1/test/app/static/img/articles'


@app.route('/articles/ajout', methods=['POST'])
def ajoutArticle():

    article = Article(nom=articlejson['nom'],categorie_id=articlejson['categorie_id'],date_publication=articlejson['date_publication'])

    db.session.add(article)
    db.session.commit()

    if ('photo' in articlejson):
        article.photo=articlejson['photo']
        db.session.commit()
        return 'succes photo'
    return 'succes'





@app.route('/articles', methods=['GET'])
def liste_article():

    articles = Article.query.all()
    table_article=[]
    for article in articles:
        json_article={
            "id_article": article.id,
            "id_categorie": article.categorie_id,
            "photo": article.photo
        }
        table_article.append(json_article)
    #envoi reponse sous forme json
    response = jsonify({"articles":table_article})
    return response.json



@app.route('/articles/sup/<int:id>', methods=['GET','POST'])
def supArticle(id):

    article = Article.query.get_or_404(id)

    db.session.delete(article)
    db.session.commit()

    return 'Suppression effectué avec succès.'





@app.route('/articles/modif/<int:id>', methods=['GET', 'POST'])
def modifArticle(id):



    article=Article.query.get_or_404(id)
    articlejson = request.get_json()
    article.nom = articlejson['nom']
    article.categorie_id = articlejson['categorie_id']
    article.date_publication = articlejson['date_publication']

    db.session.commit()



    # redirect to the roles page
    return 'Changement effectué avec succès'








app.config['UPLOADED_IMAGES_DEST'] = '/Users/bayedamethiam/Desktop/STAGE VOLKENO/test1/test/app/static/img/stocks'








@app.route('/stocks/ajout', methods=['GET', 'POST'])
def ajout_stock():


    stockjson = request.get_json()
    article=Article.query.get_or_404(stockjson['id_article'])

    db.session.add(article)
    db.session.commit()

    stock = Stock(article=article,taille=stockjson['taille'],couleur=str(stockjson['couleur']),nom_couleur=str(stockjson['nom_couleur']),prix_in=stockjson['prix'],prix_fin=stockjson['prix'] - (stockjson['prix'] *stockjson['solde'] )/100,solde=stockjson['solde'] ,quantite=stockjson['quantite'] )

    db.session.add(stock)
    db.session.commit()


    photo=Photo(id_stock=stock.id,lien_photo=stockjson['photo1'] ,position=1)
    db.session.add(photo)
    db.session.commit()

    if photo2 in stockjson :

        photo=Photo(id_stock=stock.id,lien_photo=stockjson['photo2'],position=2)
        db.session.add(photo)
        db.session.commit()


    if photo3 in stockjson :
        photo=Photo(id_stock=stock.id,lien_photo=stockjson['photo3'],position=3)
        db.session.add(photo)
        db.session.commit()



    if photo4 in stockjson :
        photo=Photo(stock=stock,lien_photo=stockjson['photo4'],position=4)
        db.session.add(photo)
        db.session.commit()

    # load department template
    return 'succès'



@app.route('/stocks/<int:id>', methods=['GET', 'POST'])
def list_stock(id):

    table_stock=[]
    stocks=db.session.query(Stock.id,Stock.id_article,Stock.prix_in,Stock.prix_fin,Stock.solde,Stock.taille,Stock.nom_couleur,Stock.couleur,Stock.quantite).filter(Stock.id_article==id).all()

    for stock in stocks:
        json_article={

            "id_stocks":stock.id,
            "id_article": stock.id_article,
            "prix_in":stock.prix_in,
            "prix_fin":stock.prix_fin,
            "solde":stock.solde,
            "taille":stock.taille,
            "nom_couleur":stock.nom_couleur,
            "couleur":stock.couleur,
            "quantite":stock.quantite

        }
        table_stock.append(json_article)
    #envoi reponse sous forme json
    response = jsonify({"stocks":table_stock})
    return response.json




@app.route('/stock/modif/<int:id>', methods=['GET', 'POST'])
def modif_stock(id):
    stock=Stock.query.get_or_404(id)


    stock.prix_in=stockjson['prix']
    stock.prix_fin=stockjson['prix'] - (stockjson['prix'] *stockjson['solde'] )/100
    stock.solde=stockjson['solde']
    stock.taille=stockjson['taille']
    stock.nom_couleur=str(stockjson['nom_couleur'])
    stock.couleur=str(stockjson['couleur'])
    stock.quantite=stockjson['quantite']


    db.session.commit()



    # load department template
    return "succes"



@app.route('/stocks/sup/<int:id>', methods=['GET', 'POST'])
def supStock(id):

    stock = Stock.query.get_or_404(id)

    db.session.delete(stock)
    db.session.commit()


    return "supression reussi"


if __name__ == '__main__':
    db.create_all()
    app.run(debug=True, host='0.0.0.0')
